import { TestBed } from '@angular/core/testing';

import { ServerErrorHandlerInterceptor } from './server-error-handler.interceptor';

describe('ServerErrorHandlerInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ServerErrorHandlerInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ServerErrorHandlerInterceptor = TestBed.inject(ServerErrorHandlerInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
