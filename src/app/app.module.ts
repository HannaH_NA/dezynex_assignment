import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { LayoutModule } from './views/layout/layout.module';
import { AuthGuard } from './core/guard/auth.guard';

import { AppComponent } from './app.component';
import { ErrorPageComponent } from './views/pages/error-page/error-page.component';

import { HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { ServerErrorHandlerInterceptor } from './server-error-handler.interceptor';
import { CommonModule } from '@angular/common';

// import { LoginComponent } from './views/pages/auth/login/login.component';
// import { RegisterComponent } from './views/pages/auth/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    // RegisterComponent,
    // LoginComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    MatSelectCountryModule.forRoot('en')
  ],
  providers: [ ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
