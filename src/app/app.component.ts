import { Component, OnInit } from '@angular/core';
import { AuthService } from './views/pages/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Delisys-admin-web';


  constructor(
    private authService: AuthService
  ) { }
  
  ngOnInit(): void {
    this.authService.autoLogin();

  }

}
