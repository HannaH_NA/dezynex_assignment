import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import {AuthService } from '../auth.service';
import { User } from '../user.mode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('operationForm') public operationForm: NgForm;

  constructor(private authService: AuthService, private router: Router) { }
  loading: boolean = false;
  hasError: boolean = false;

  ngOnInit(): void {
  }

  onSubmit(loginForm: NgForm) {
    console.log("-------------------", loginForm.valid);

    if (!loginForm.valid) {
      return;
    }
    // this.authService.
    this.loading = true;
    console.log("SUBMITTED");

    const email = loginForm.value.email;
    const password = loginForm.value.password;

    let authObs: Observable<User>;

    this.authService.login(email, password).subscribe(
      (res) => {
        this.loading = false;
        this.router.navigate(['/profile']);
      },
      (error) => {
        console.log(error.messages);
        this.hasError = true;
        this.loading = false;
      }
    );
  }

}