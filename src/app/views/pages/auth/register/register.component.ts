import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import {AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild('operationForm') public operationForm: NgForm;

  constructor(private authService: AuthService, private router: Router) { }
  loading: boolean = false;
  hasError: boolean = false;

  ngOnInit(): void {
  }

  onSubmit(registerForm: NgForm) {
    console.log("-------------------", registerForm.valid);

    if (!registerForm.valid) {
      return;
    }
    // this.authService.
    this.loading = true;
    console.log("SUBMITTED");

    // const email = "admin";
    // const password = "Admin@2020";
    const email = registerForm.value.email;
    const password = registerForm.value.password;
    const username = registerForm.value.username;
    // const reEnterPassword = registerForm.value.reEnterPassword;
    console.log(email);


    this.authService.register(registerForm.value).subscribe(
      res => {
        console.log("++++++",res);
        
        this.loading = false;
        this.router.navigate(['/auth/login']);
      }, error => {
        console.log(error);
        this.hasError = true;
        this.loading = false;
      }
    );
  }

}