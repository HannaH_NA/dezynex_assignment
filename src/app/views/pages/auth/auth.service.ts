import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { tap,catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from './user.mode';

export interface AuthResponseData {
  id: string;
  name: string;
  email: string;
  phone:string;
  token: string;
  refreshTocken : string;
  expiresIn? : string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // user = new Subject<User>();
  user = new BehaviorSubject<User>(null);

  authBaseUrl = environment.baseUrl;
  constructor(private http: HttpClient, private router: Router) { }

  register(data): Observable<any> {
    let url = `${this.authBaseUrl}/register`;
    return this.http.post(url, data).pipe();
  }

  login(email: string, password: string){
    let data = {
      // email: email,
      email: email,
      password: password
    };

    return this.http
      .post<any>(this.authBaseUrl+"/login",data)
      .pipe(
        tap(resData => {
          let data = resData.data;
          console.log("????????????",data);
          
          this.handleAuthentication(data._id,data.email,data.username,data.token)
        })
      );
  }

  autoLogin(){
    // console.log("Getting auto login");
    const userData: {
      id: string;
      username: string;
      email: string;
      _token: string;
      refreshTocken : string;
      expiresIn? : string;
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return null;
    }
    const loadedUser = new User(
      userData.id,
      userData.email,
      userData.username,
      userData._token,
      );
      console.log("PARSE DATA : ",userData,loadedUser);
      if (loadedUser.token) {
        this.user.next(loadedUser);
      // const expirationDuration =
      //   new Date(userData._tokenExpirationDate).getTime() -
      //   new Date().getTime();
      // this.autoLogout(expirationDuration);
    }
  }

  logout() {
    this.user.next(null);
    localStorage.removeItem('userData');
    this.router.navigate(['/auth/login']);
    // if (this.tokenExpirationTimer) {
    //   clearTimeout(this.tokenExpirationTimer);
    // }
    // this.tokenExpirationTimer = null;
  }

  private handleAuthentication(id:string, email: string, phone: string, token: string){
    const user = new User(id,email,phone,token);
    this.user.next(user);
    //Write Auto logout codes here
    localStorage.setItem('userData',JSON.stringify(user));
  }

  forgotPassword(email: string){
    let data = {
      email: email,
    };
    return this.http.post<any>(this.authBaseUrl+"/api/forgot_password",data);
  }

  verifyOtp(email: string, otp: string){
    let data = {
      otp: otp,
      password: otp,
      // email: email,
    };
    return this.http.post<any>(this.authBaseUrl+"/api/otp_verification/"+email,data);
  }

  resetPassword(email: string,password: string){
    let data = {
      email: email,
      password: password
    };
    return this.http.put<any>(this.authBaseUrl+"/api/otp_reset_password",data);
  }


}
