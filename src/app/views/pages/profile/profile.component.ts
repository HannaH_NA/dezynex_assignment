import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import {ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('operationForm') public operationForm: NgForm;

  constructor(private profileService: ProfileService, private router: Router) { }
  loading: boolean = false;
  hasError: boolean = false;
  users: any[] = null;
  user: any;

  ngOnInit(): void {
    var user = JSON.parse(localStorage.getItem('userData'));
console.log("888888888888",user);

    this.loadAddress(user.id);
  }

  loadAddress(id) {

    this.profileService.getAll(id).subscribe(res => {
      this.user = res.result      
    })
  }

  onSubmit(profileForm: NgForm) {
    console.log("-------------------", profileForm.valid);

    if (!profileForm.valid) {
      return;
    }
    // this.authService.
    this.loading = true;
    console.log("SUBMITTED");

    // const email = "admin";
    // const password = "Admin@2020";
    const email = profileForm.value.email;
    const password = profileForm.value.password;
    const username = profileForm.value.username;
    // const reEnterPassword = loginForm.value.reEnterPassword;
    console.log(email);
    var data={
       firstname : profileForm.value.firstname,
       lastname : profileForm.value.lastname,
       email : profileForm.value.email,
       contactnumber : profileForm.value.contactnumber,
       companyname : profileForm.value.companyname,
       address : profileForm.value.address,
       city : profileForm.value.city,
       state : profileForm.value.state,
       country : profileForm.value.country,
       userid: this.user._id

    }
console.log("ddaaaatttaaa:  ",data);


    this.profileService.profileSubmit(data).subscribe(
      res => {
        console.log("+///////////+",res);
        
        this.loading = false;
        alert("Save Profile")
      }, error => {
        console.log("666666",error);
        this.hasError = true;
        this.loading = false;
      }
    );
  }

}