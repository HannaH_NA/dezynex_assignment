import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
  {
    label: 'Main',
    isTitle: true
  },
  {
    label: 'Dashboard',
    icon: 'home',
    link: '/dashboard'
  },
  {
    label: 'Settings',
    isTitle: true,
  },
  {
    label: 'Profile',
    icon: 'user',
    link: '/profile'
  },
  {
    label: 'Customize',
    icon: 'edit',
    link: '/customize'
  },
  {
    label: 'Featured Listing',
    icon: 'layout',
    link: '/featured-listing'
  },
  {
    label: 'User Privilege',
    icon: 'layout',
    link: '/privilege'
  },
  {
    label: 'CMS',
    icon: 'globe',
    subItems: [
      {
        label: 'Pages',
        link: '/cms/pages',
      },
      {
        label: 'Email',
        link: '/cms/mail'
      },
      {
        label: 'Notifications',
        link: '/cms/notification'
      },
    ]
  },
  {
    label: 'Catalog',
    icon: 'book',
    subItems: [
      {
        label: 'Category',
        link: '/catalog/category',
      },
      {
        label: 'Variant',
        link: '/catalog/variant'
      },
      {
        label: 'Brand',
        link: '/catalog/brand'
      }
    ]
  },
  {
    label: 'Configuration',
    icon: 'settings',
    link: '/configuration'
  },
  {
    label: 'Tax',
    icon: 'dollar-sign',
    subItems: [
      {
        label: 'Tax Category',
        link: '/tax/tax-category',
      },
      {
        label: 'Tax Rate',
        link: '/tax/tax-rate'
      }
    ]
  },
  {
    label: 'MARKETING',
    isTitle: true
  },
  {
    label: 'Promo Code',
    icon: 'gift',
    link: '/promocode'
  },
  {
    label: 'MANAGEMENT',
    isTitle: true
  },
  {
    label: 'Restaurant Owner ',
    icon: 'briefcase',
    link: '/management/restownmng'
  },
  {
    label: 'Store',
    icon: 'archive',
    link: '/management/restmng'
  },
  {
    label: 'Customer ',
    icon: 'users',
    link: '/management/custmng'
  },
  {
    label: 'Admin Delivery',
    icon: 'user-check',
    link: '/management/admindel'
  },
  {
    label: 'Delivery Boy',
    icon: 'map-pin',
    link: '/management/delboy'
  },
  {
    label: 'Review',
    icon: 'star',
    subItems: [
      {
        label: 'Item Review',
        link: '/management/revmng/itemreview',
      },
      {
        label: 'Restaurant Review',
        link: '/management/revmng/restreview'
      },
      {
        label: 'Order Review',
        link: '/management/revmng/orderreview'
      },
    ]
  },
  {
    label: 'Failed Orders',
    icon: 'thumbs-down',
    link: '/management/failedorder'
  },
  {
    label: 'Stock Management',
    icon: 'database',
    link: '/management/stockmng'
  },
  {
    label: 'Sub Admin',
    icon: 'user-minus',
    link: '/management/subadmin'
  },
  {
    label: 'FAQ',
    icon: 'help-circle',
    link: '/management/frequentlyaskedqstn'
  },
  {
    label: 'Banner Management',
    icon: 'tv',
    link: '/management/bannermng'
  },
  {
    label: 'Special Banner Ads',
    icon: 'tv',
    link: '/management/spclbannermng'
  },
  {
    label: 'COMMISSION TRACKING',
    isTitle: true
  },
  {
    label: 'Merchant Commission ',
    icon: 'anchor',
    link: '/commission/merchantcommissiontrack'
  },
  {
    label: 'Delivery Boy Commission ',
    icon: 'truck',
    link: '/commission/deliveryboycommissiontrack'
  },
  {
    label: 'Payment',
    isTitle: true
  },
  {
    label: 'Invoices',
    icon: 'file',
    link: '/invoice'
  },
  {
    label: 'Payment Cancellation',
    icon: 'zap-off',
    link: '/paymentcancel'
  },
  {
    label: 'Refer Friend',
    icon: 'smile',
    link: '/referfriend'
  },
  {
    label: 'Payment Options',
    icon: 'zap',
    link: '/paymentoptions'
  },
  {
    label: 'Store Payout',
    icon: 'shopping-bag',
    link: '/storepayout'
  },
  {
    label: 'Reports',
    isTitle: true
  },
  {
    label: 'Order Report',
    icon: 'bar-chart',
    link: '/reports/orderreport'
  },
  {
    label: 'Transaction Report',
    icon: 'bar-chart-2',
    link: '/reports/transreport'
  },
  {
    label: 'Sales Report',
    icon: 'percent',
    link: '/reports/salesreport'
  },
  {
    label: 'OTHERS',
    isTitle: true
  },
  {
    label: 'Other Settings',
    icon: 'settings',
    link: '/others/othersettings'
  },
  {
    label: 'SignUps',
    icon: 'user-plus',
    link: '/others/signup'
  },
  {
    label: 'Logout',
    icon: 'log-out',
    link: '/auth/login'
  }
  // {
  //   label: 'Web Apps',
  //   isTitle: true
  // },
  // {
  //   label: 'Email',
  //   icon: 'mail',
  //   subItems: [
  //     {
  //       label: 'Inbox',
  //       link: '/apps/email/inbox',
  //     },
  //     {
  //       label: 'Read',
  //       link: '/apps/email/read'
  //     },
  //     {
  //       label: 'Compose',
  //       link: '/apps/email/compose'
  //     },
  //   ]
  // },
  // {
  //   label: 'Chat',
  //   icon: 'message-square',
  //   link: '/apps/chat',
  // },
  // {
  //   label: 'Calendar',
  //   icon: 'calendar',
  //   link: '/apps/calendar',
  //   badge: {
  //     variant: 'primary',
  //     text: 'Event',
  //   }
  // },
  // {
  //   label: 'Components',
  //   isTitle: true
  // },
  // {
  //   label: 'UI Kit',
  //   icon: 'feather',
  //   subItems: [
  //     {
  //       label: 'Accordion',
  //       link: '/ui-components/accordion',
  //     },
  //     {
  //       label: 'Alerts',
  //       link: '/ui-components/alerts',
  //     },
  //     {
  //       label: 'Badges',
  //       link: '/ui-components/badges',
  //     },
  //     {
  //       label: 'Breadcrumbs',
  //       link: '/ui-components/breadcrumbs',
  //     },
  //     {
  //       label: 'Buttons',
  //       link: '/ui-components/buttons',
  //     },
  //     {
  //       label: 'Button group',
  //       link: '/ui-components/button-group',
  //     },
  //     {
  //       label: 'Cards',
  //       link: '/ui-components/cards',
  //     },
  //     {
  //       label: 'Carousel',
  //       link: '/ui-components/carousel',
  //     },
  //     {
  //       label: 'Collapse',
  //       link: '/ui-components/collapse',
  //     },
  //     {
  //       label: 'Datepicker',
  //       link: '/ui-components/datepicker',
  //     },
  //     {
  //       label: 'Dropdowns',
  //       link: '/ui-components/dropdowns',
  //     },
  //     {
  //       label: 'List group',
  //       link: '/ui-components/list-group',
  //     },
  //     {
  //       label: 'Media object',
  //       link: '/ui-components/media-object',
  //     },
  //     {
  //       label: 'Modal',
  //       link: '/ui-components/modal',
  //     },
  //     {
  //       label: 'Navs',
  //       link: '/ui-components/navs',
  //     },
  //     {
  //       label: 'Navbar',
  //       link: '/ui-components/navbar',
  //     },
  //     {
  //       label: 'Pagination',
  //       link: '/ui-components/pagination',
  //     },
  //     {
  //       label: 'Popovers',
  //       link: '/ui-components/popovers',
  //     },
  //     {
  //       label: 'Progress',
  //       link: '/ui-components/progress',
  //     },
  //     {
  //       label: 'Rating',
  //       link: '/ui-components/rating',
  //     },
  //     {
  //       label: 'Scrollbar',
  //       link: '/ui-components/scrollbar',
  //     },
  //     {
  //       label: 'Spinners',
  //       link: '/ui-components/spinners',
  //     },
  //     {
  //       label: 'Timepicker',
  //       link: '/ui-components/timepicker',
  //     },
  //     {
  //       label: 'Tooltips',
  //       link: '/ui-components/tooltips',
  //     },
  //     {
  //       label: 'Typeadhed',
  //       link: '/ui-components/typeahead',
  //     },
  //   ]
  // },
  // {
  //   label: 'Advanced UI',
  //   icon: 'anchor',
  //   subItems: [
  //     {
  //       label: 'Cropper',
  //       link: '/advanced-ui/cropper',
  //     },
  //     {
  //       label: 'Owl carousel',
  //       link: '/advanced-ui/owl-carousel',
  //     },
  //     {
  //       label: 'Sweet alert',
  //       link: '/advanced-ui/sweet-alert',
  //     },
  //   ]
  // },
  // {
  //   label: 'Forms',
  //   icon: 'file-text',
  //   subItems: [
  //     {
  //       label: 'Basic elements',
  //       link: '/form-elements/basic-elements'
  //     },
  //     {
  //       label: 'Advanced elements',
  //       subItems: [
  //         {
  //           label: 'Form validation',
  //           link: '/advanced-form-elements/form-validation'
  //         },
  //         {
  //           label: 'Input mask',
  //           link: '/advanced-form-elements/input-mask'
  //         },
  //         {
  //           label: 'Ng-select',
  //           link: '/advanced-form-elements/ng-select'
  //         },
  //         {
  //           label: 'Ngx-chips',
  //           link: '/advanced-form-elements/ngx-chips'
  //         },
  //         {
  //           label: 'Ngx-color-picker',
  //           link: '/advanced-form-elements/ngx-color-picker'
  //         },
  //         {
  //           label: 'Ngx-dropzone',
  //           link: '/advanced-form-elements/ngx-dropzone-wrapper'
  //         },
  //       ]
  //     },
  //     {
  //       label: 'Editors',
  //       link: '/form-elements/editors'
  //     },
  //     {
  //       label: 'Wizard',
  //       link: '/form-elements/wizard'
  //     },
  //   ]
  // },
  // {
  //   label: 'Charts & graphs',
  //   icon: 'pie-chart',
  //   subItems: [
  //     {
  //       label: 'ApexCharts',
  //       link: '/charts-graphs/apexcharts',
  //     },
  //     {
  //       label: 'ChartJs',
  //       link: '/charts-graphs/chartjs',
  //     },
  //   ]
  // },
  // {
  //   label: 'Tables',
  //   icon: 'layout',
  //   subItems: [
  //     {
  //       label: 'Basic tables',
  //       link: '/tables/basic-table',
  //     },
  //     {
  //       label: 'Data table',
  //       link: '/tables/data-table',
  //     },
  //     {
  //       label: 'Ngx-datatable',
  //       link: '/tables/ngx-datatable'
  //     }
  //   ]
  // },
  // {
  //   label: 'Icons',
  //   icon: 'smile',
  //   subItems: [
  //     {
  //       label: 'Feather icons',
  //       link: '/icons/feather-icons',
  //     },
  //     {
  //       label: 'Mdi icons',
  //       link: '/icons/mdi-icons',
  //     }
  //   ]
  // },
  // {
  //   label: 'Pages',
  //   isTitle: true
  // },
  // {
  //   label: 'Special pages',
  //   icon: 'book',
  //   subItems: [
  //     {
  //       label: 'Blank page',
  //       link: '/general/blank-page',
  //     },
  //     {
  //       label: 'Faq',
  //       link: '/general/faq',
  //     },
  //     {
  //       label: 'Invoice',
  //       link: '/general/invoice',
  //     },
  //     {
  //       label: 'Profile',
  //       link: '/general/profile',
  //     },
  //     {
  //       label: 'Pricing',
  //       link: '/general/pricing',
  //     },
  //     {
  //       label: 'Timeline',
  //       link: '/general/timeline',
  //     }
  //   ]
  // },
  // {
  //   label: 'Authentication',
  //   icon: 'unlock',
  //   subItems: [
  //     {
  //       label: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       label: 'Register',
  //       link: '/auth/register',
  //     },
  //   ]
  // },
  // {
  //   label: 'Error',
  //   icon: 'cloud-off',
  //   subItems: [
  //     {
  //       label: '404',
  //       link: '/error/404',
  //     },
  //     {
  //       label: '500',
  //       link: '/error/500',
  //     },
  //   ]
  // },
];

export const DELIVERY_MANAGER_MENU: MenuItem[] = [
  {
    label: 'Main',
    isTitle: true
  },
  {
    label: 'Dashboard',
    icon: 'home',
    link: '/dashboard'
  },
  {
    label: 'Settings',
    isTitle: true,
  },
  {
    label: 'Profile',
    icon: 'user',
    link: '/deliverymanager/deliveryprofile'
  },
  {
    label: 'General Settings',
    icon: 'layout',
    link: '/deliverymanager/generalsettings'
  },
  {
    label: 'Customize',
    icon: 'edit',
    link: '/deliverymanager/customize'
  },
  {
    label: 'Teams',
    icon: 'users',
    link: '/deliverymanager/teams'
  },
  {
    label: 'Agents',
    icon: 'user-check',
    link: '/deliverymanager/agents'
  },
  {
    label: 'Payout Request',
    icon: 'archive',
    link: '/deliverymanager/payoutrequest'
  },
  // {
  //   label: 'Customize',
  //   icon: 'globe',
  //   subItems: [
  //     {
  //       label: 'CMS',
  //       link: '/deliverymanager/customize/cms',
  //     },
  //     {
  //       label: 'Nomenclature',
  //       link: '/deliverymanager/customize/nomenclature'
  //     },
  //     {
  //       label: 'Date & Time',
  //       link: '/deliverymanager/customize/date'
  //     },
  //     {
  //       label: 'Task Completion Proof',
  //       link: '/deliverymanager/customize/taskproof'
  //     },
  //   ]
  // },
  {
    label: 'Logout',
    icon: 'log-out',
    link: '/auth/login'
  }
];
