export interface Restaurant {
    id: string;
    picture: string;
    name: string;
    email: string;
    phone: string;
}

export interface DeliveryBoy {
    id: string;
    picture: string;
    name: string;
    email: string;
    phone: string;
}