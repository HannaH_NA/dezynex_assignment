const jwt = require("jsonwebtoken");

module.exports = {
  userAuthCheck: function (req, res, next) {
    if (!req.cookies.test) {
      return res.status(401).send({ error: "Token missing" });
    }

    var token = req.cookies.test.split(" ")[0];

    jwt.verify(token, "secretKey", function (err, decoded) {
      if (err)
        return res.status(500).json({
          success: false,
          error: {
            type: "Authentication Failed"
          }
        });

      req.headers.currentUser = decoded;
      next();
    });
  }
};
