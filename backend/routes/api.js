var express = require('express');
var router = express.Router();
const userController = require("../controllers/user");
const profileController = require("../controllers/profile");

const authHelper = require("../helpers/auth");

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/profile", profileController.addProfile);
router.get("/getUser/:id", profileController.viewProfile);


module.exports = router;
