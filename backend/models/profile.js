const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var profileSchema = new Schema({
    userid: [{type: mongoose.Schema.Types.ObjectId, ref: "User"}],
    firstname: { type: String, require: true },
    lastname: { type: String, require: true },
    contactnumber: { type: Number, require: true },
    companyname: { type: String, require: true },
    email: { type: String, require: true },
    address: { type: String, require: true },
    city: { type: String, require: true },
    state: { type: String, require: true },
    country: { type: String, require: true },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Number, required: false }
});
profileSchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("Profile", profileSchema);
