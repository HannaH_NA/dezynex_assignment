const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var userSchema = new Schema({
    username: { type: String, require: true },
    email: { type: String, require: true },
    password: { type: String, require: true },
    createdAt: { type: Date, required: false },
    updatedAt: { type: Number, required: false }
});
userSchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("User", userSchema);
