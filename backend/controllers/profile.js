const mongoose = require("mongoose");
const profileSchema = require("../models/profile");
const Profile = mongoose.model("Profile");
const userSchema = require("../models/user");
const User = mongoose.model("User");

const profileController = {
  addProfile: function (req, res) {
    data = req.body;
    console.log(data);

    // Profile.findOne({ profile: data.profile })
    //   .then((response) => {
    //     console.log("test1");

        // if (response == null) {
          const profile = new Profile(data);
          profile
            .save()
            .then((result) => {
              console.log("profile save to database" + result);
              return res.status(200).json({
                success: true,
                result: result,
              });
            })
            .catch((err) => {
              return res.status(422).json({
                success: false,
                error: err,
              });
            });
        // } else {
        //   return res.status(422).json({
        //     success: false,
        //     error: "profile already exist",
        //   });
        // }
    //   })
    //   .catch((error) => {
    //     return res.status(422).json({
    //       success: false,
    //       error: error,
    //     });
    //   });
  },
  viewProfile: (req, res) => {
    console.log("-----------",req.params.id);
    User.findById(req.params.id)
      .then((result) => {
        console.log("view data", result);
        return res.status(200).json({
          success: true,
          result: result,
        });
      })
      .catch((err) => {
        return res.status(422).json({
          success: false,
          error: err,
        });
      });
  },
//   viewProfiles: (req, res) => {
//     Profile.find()
//       .then((result) => {
//         console.log("view data", result);
//         return res.status(200).json({
//           success: true,
//           result: result,
//         });
//       })
//       .catch((err) => {
//         return res.status(422).json({
//           success: false,
//           error: err,
//         });
//       });
//   },

//   editProfile: (req, res) => {
//     Profile.findOne({ name: req.body.name }).then((response) => {
//       if (response == null) {
//         Profile.findByIdAndUpdate(req.params.id, { $set: req.body })
//           .then((result) => {
//             if (result) {
//               return res.status(200).json({
//                 success: true,
//                 result: "updated ",
//               });
//             } else {
//               return res.status(422).json({
//                 success: false,
//                 result: "error",
//               });
//             }
//           })
//           .catch((err) => {
//             return res.status(422).json({
//               success: false,
//               result: err,
//             });
//           });
//       } else {
//         return res.status(422).json({
//           success: false,
//           error: "already exist ",
//         });
//       }
//     });
//   },
//   deleteProfile: (req, res) => {
//     Profile.findByIdAndRemove(req.params.id)
//       .then((response) => {
//         if (response) {
//           return res.status(200).json({
//             success: true,
//             result: "Deleted ",
//           });
//         } else {
//           return res.status(422).json({
//             success: false,
//             result: "profile not exist ",
//           });
//         }
//       })
//       .catch((error) => {
//         return res.status(422).json({
//           success: false,
//           result: error,
//         });
//       });
//   }
};

module.exports = profileController;
